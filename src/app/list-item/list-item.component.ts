import { Component, OnInit ,Input} from '@angular/core';

@Component({
  selector: 'listitem',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css'],

})
export class ListItemComponent implements OnInit {
  @Input () data:any; //
  id;
  title;
  price;
  stock;
 

  constructor() { }
  
  ngOnInit() //כל אלמנט טודו שנוצר מפעיל את הפונקציה
  {
    this.id= this.data.id;//העברה של איידי
    this.title= this.data.title; 
    this.price= this.data.price; 
    this.stock= this.data.stock; 

  }

}
